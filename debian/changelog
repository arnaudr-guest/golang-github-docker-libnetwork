golang-github-docker-libnetwork (0.8.0-dev.2+git20180113.fcf1c3b~ds1-1) unstable; urgency=medium

  [ Alexandre Viau ]
  * Point Vcs-* urls to salsa.debian.org.

  [ Konstantinos Margaritis ]
  * Replace golang-go with golang-any in Build-Depends, remove golang-go from Depends
  * add golang-github-pkg-errors-dev dependency
  * special case ppc64, builds only with gccgo-go

  [ Dmitry Smirnov ]
  * Patch sirupsen/logrus import path.
  * Standards-Version: 4.1.4; Priority: optional.

  [ Arnaud Rebillout ]
  * Bump compat
  * Update control file
  * Update dependencies and copyright for version 0.8.0-dev.2+git20180113.fcf1c3b
  * Cleanup rules a bit
  * Remove logrus-lowercase patch
  * Fix build for go 1.10
  * Fix install rules
  * Fix pristine-tar in gpb.conf
  * Add myself to uploaders

 -- Arnaud Rebillout <arnaud.rebillout@collabora.com>  Sat, 09 Jun 2018 00:15:54 +0700

golang-github-docker-libnetwork (0.8.0-dev.2+git20170202.599.45b4086-3) unstable; urgency=medium

  * Special case ppc64, builds only with gccgo-go (golang-go does not support
    shared build/linking for ppc64)

 -- Konstantinos Margaritis <markos@debian.org>  Fri, 11 Aug 2017 09:50:42 +0300

golang-github-docker-libnetwork (0.8.0-dev.2+git20170202.599.45b4086-2) unstable; urgency=medium

  * Replace golang-go with golang-any in Build-Depends, remove golang-go from Depends

 -- Konstantinos Margaritis <markos@debian.org>  Wed, 09 Aug 2017 17:23:26 +0300

golang-github-docker-libnetwork (0.8.0-dev.2+git20170202.599.45b4086-1) unstable; urgency=medium

  * New upstream snapshot for Docker 1.13.1.
  * Delete patches vendoring various parts of Docker and replace with
    golang-github-docker-docker-dev B-D.

 -- Tim Potter <tpot@hpe.com>  Wed, 24 May 2017 11:17:34 +1000

golang-github-docker-libnetwork (0.8.0~dev.2+git20161130.568.fd27f22-4) unstable; urgency=medium

  * Tighten B-D on golang-github-vishvananda-netlink-dev package for
    stretch build. (Closes: #858267)
  * Suppress binary-without-manpage Lintian warnings.

 -- Tim Potter <tpot@hpe.com>  Tue, 21 Mar 2017 08:40:08 +1100

golang-github-docker-libnetwork (0.8.0~dev.2+git20161130.568.fd27f22-3) unstable; urgency=medium

  * Add Breaks line to binary package to avoid messing up previous
    Docker installs.

 -- Tim Potter <tpot@hpe.com>  Fri, 24 Feb 2017 10:18:43 +1100

golang-github-docker-libnetwork (0.8.0~dev.2+git20161130.568.fd27f22-2) unstable; urgency=medium

  * Build and package command line utilites, including docker-proxy.
  * Package documentation files.

 -- Tim Potter <tpot@hpe.com>  Thu, 16 Feb 2017 07:35:38 +1100

golang-github-docker-libnetwork (0.8.0~dev.2+git20161130.568.fd27f22-1) unstable; urgency=medium

  * New upstream snapshot.

 -- Tim Potter <tpot@hpe.com>  Wed, 25 Jan 2017 14:22:41 +1100

golang-github-docker-libnetwork (0.8~git20161019.0.66c8446-1) unstable; urgency=medium

  * New upstream snapshot.
  * Add golang-github-docker-go-events build dependency.
  * Remove patch now applied upstream.
  * TODO: dfsg-ify upstream tarball

 -- Tim Potter <tpot@hpe.com>  Wed, 30 Nov 2016 15:36:05 +1100

golang-github-docker-libnetwork (0.7.2~rc.1+dfsg-1) unstable; urgency=medium

  [ Paul Tagliamonte ]
  * Use a secure transport for the Vcs-Git and Vcs-Browser URL

  [ Dmitry Smirnov ]
  * New upstream version.
  * New patch to fix FTBFS with libkv v0.2.0 (Closes: #830656).
    Thanks, Tianon Gravi.

 -- Dmitry Smirnov <onlyjob@debian.org>  Sun, 10 Jul 2016 09:38:37 +1000

golang-github-docker-libnetwork (0.7.0~rc.6+dfsg-1) unstable; urgency=medium

  [ Tim Potter <tpot@hpe.com> ]
  * New upstream version (Closes: #823466)
  * Remove duplicate entry in d/watch file
  * Standards-Version: 3.9.8
  * Remove unused copy of MPL from d/copyright

  [ Dmitry Smirnov <onlyjob@debian.org> ]
  * Build-Depends:
    - golang-github-hashicorp-consul-dev
    - golang-dns-dev | golang-github-miekg-dns-dev
    + golang-github-docker-go-connections-dev

 -- Dmitry Smirnov <onlyjob@debian.org>  Wed, 11 May 2016 09:24:06 +1000

golang-github-docker-libnetwork (0.5.6+dfsg-1) unstable; urgency=medium

  * Initial release (Closes: #816884).

 -- Dmitry Smirnov <onlyjob@debian.org>  Sat, 12 Mar 2016 12:39:51 +1100
